<?php
 
class Enterweb_AfterOrder_Model_Observer {
 
 	public $order;
    
    public function afterOrderSave($event)
    {
    	$order = $event->getEvent()->getOrder();
    	$this->order = Mage::getModel('sales/order')->load($order->getId()); 
    	$status = $this->order->getStatus();
    	
    	if($status == 'complete' || $status == 'payed')
    	{
    		if(!$this->order->hasInvoices()){
    			$this->createInvoice();
    		}
    		if(!$this->order->hasShipments()){
    			$this->createShipment();
    		}
    	}
    	return $this;
    }
    
    public function createInvoice() 
    {
    	if ($this->order->getId()) 
    	{
	        if ($this->order->canInvoice()) 
	        {
	            $this->order->getPayment()->setSkipTransactionCreation(false);
	            $invoice = $this->order->prepareInvoice();
	            $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
	            $invoice->register();
	            Mage::getModel('core/resource_transaction')
	                   ->addObject($invoice)
	                   ->addObject($this->order)
	                   ->save();

		        $invoice->sendEmail();
	        }
	        else 
	        {
            	Mage::log('cannot invoice',null,'cannotinvoice_'.$this->order->getId().'.log');
	        }
    	}
    	return $this;
    }
    
    public function createShipment()
    {
    	try {
    		
    		if($this->order->canShip())
		    {
    			
    			//Create shipment
    			$shipmentid = Mage::getModel('sales/order_shipment_api')->create($this->order->getIncrementId(), array());
    			
    			//Add tracking information
     			$ship = Mage::getModel('sales/order_shipment_api')->addTrack($this->order->getIncrementId(), array());
    			
    		}
    		else
    		{
    			Mage::log('cannot ship',null,'cannotship_'.$this->order->getId().'.log');
    		}
    	}catch (Mage_Core_Exception $e) {
    		print_r($e);
    	}

    	return $this;
    }
}
 
?>