<?php
class Enterweb_Gmap_Helper_Gmap extends Mage_Core_Helper_Abstract
{
  
    const GOOGLE_MAPS_HOST = 'maps.google.com';
    const CONFIG_PATH_GOOGLE_MAPS_API_KEY = 'inchoo_google/maps/api_key';
    public function getGoogleMapsApiKey()
    {
        return Mage::getStoreConfig(self::CONFIG_PATH_GOOGLE_MAPS_API_KEY);
    }
    
	
    public function fetchAddressGeoCoordinates(Mage_Customer_Model_Address $address, $saveCoordinatesToAddress = true, $forceLookup = false)
    {
   
        $coordinates = array();
        if($address->getId() && $address->getGmapCoordinatePointX() && $address->getGmapCoordinatePointY() && $forceLookup == false) 
        {
            $coordinates = array($address->getGmapCoordinatePointX(), $address->getGmapCoordinatePointY());
        } 
        elseif(
                (!$address->getGmapCoordinatePointX() && !$address->getGmapCoordinatePointY()) 
                || ($address->getId() && $address->getGmapCoordinatePointX() && $address->getGmapCoordinatePointY() && $forceLookup == true)) 
        {
            $lineAddress = $address->getStreet1(). ', '.$address->getPostcode().' '.$address->getCity().', '.$address->getCountry();
            $client = new Zend_Http_Client();
            $client->setUri('http://'.self::GOOGLE_MAPS_HOST.'/maps/geo');
            $client->setMethod(Zend_Http_Client::GET);
            $client->setParameterGet('output', 'json');
            $client->setParameterGet('key', $this->getGoogleMapsApiKey());
            $client->setParameterGet('q', $lineAddress);
            $response = $client->request();
            if ($response->isSuccessful() && $response->getStatus() == 200) {
                $_response = json_decode($response->getBody());
                $_coordinates = @$_response->Placemark[0]->Point->coordinates;
                if (is_array($_coordinates) && count($_coordinates) >= 2) {
                    $coordinates = array_slice($_coordinates, 0, 2);
                    if ($saveCoordinatesToAddress) {
                        try {
                            $address->setLng($coordinates[0]);
                            $address->setLat($coordinates[1]);
                            $address->save();
                        } catch (Exception $e) {
                            Mage::logException($e);
                        }
                    }
                }
            }
        }
      
        return $coordinates;
    }

}