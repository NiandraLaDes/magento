<?php
class Enterweb_Gmap_Model_Observer
{
    public function geocodeAddress($observer)
    {
        echo $observer->getEvent()->getCustomerAddress();
        Mage::helper('enterweb/gmap')->fetchAddressGeoCoordinates($observer->getEvent()->getCustomerAddress());
        return $this;
    }
}