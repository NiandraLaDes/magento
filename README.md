# Project Title

Magento plugins

# Description

A couple of magento plugins:

- SkipShipping: For skipping shipping step in checkout
- Gmap: Integrate google maps

## Built With

* [Magento](https://magento.com/) - An open-source e-commerce platform written in PHP


## Authors

* **Bart van Enter** - *Initial work* - [Bitbucket](https://bitbucket.org/NiandraLaDes) and [Enter Webdevelopment](https://enter-webdevelopment.nl/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details